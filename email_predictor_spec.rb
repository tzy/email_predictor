require_relative 'email_predictor'

describe 'email predictor:' do
  let(:no_pattern_found_info) {
    'email address cannot be predicted; No email with domain name: bad.com found in dataset'
  }
  describe '#get_name_patterns' do
    let(:pattern_list) {[
      :first_name_dot_last_name, 
      :first_name_dot_last_initial,
      :first_initial_dot_last_name,
      :first_initial_dot_last_initial  
    ]}
    it "accesses the 'email_data' sample-data hash" do
      $email_data['test name'] = 'test.n@test.com'
      expect(get_name_patterns('test.com')).to eq([pattern_list[1]])
      $email_data.delete('test name')
    end

    it 'determines single email-format pattern for a given domain' do
      expect(get_name_patterns('alphasights.com').length).to eq(1)
      expect(get_name_patterns('alphasights.com')).to eq([pattern_list[0]])
    end

    it 'determines multiple email-format pattern for a given domain' do
      expect(get_name_patterns('google.com').length).to eq(2)
      expect(get_name_patterns('google.com')).to eq([pattern_list[1], pattern_list[2]])
    end

    it 'returns an error info when no email-format pattern is found for the domain' do
      expect(get_name_patterns('bad.com')).to eq(no_pattern_found_info)
    end    
  end

  describe '#predict_email' do
    let(:expected_email_address) { 'peter.wong@alphasights.com' }
    let(:expected_email_addresses) { 'craig.s@google.com, c.silverstein@google.com' }

    it 'predicts the email address from single name pattern' do
      expect(predict_email('Peter Wong', 'alphasights.com')).to eq(expected_email_address)
    end

    it 'predicts the email address from multiple name pattern' do
      expect(predict_email('Craig Silverstein', 'google.com')).to eq(expected_email_addresses)
    end

    it 'returns an error info when no name pattern is found in the database' do
      expect(predict_email('fake name', 'bad.com')).to eq(no_pattern_found_info)
    end
  end

  describe '#predict_all_emails' do
    let(:fake_advisors) {
      {
        'Tim Cooker' => 'apple.com',
        'Jim Li' => 'google.com',
        'Joe lecox' => 'alphasights.com'
      }
    }
    let(:fake_advisors_with_mismatch_domain) {
      {
        'Tim Cooker' => 'apple.com',
        'Jim Li' => 'google.com',
        'Joe lecox' => 'alphasights.com',
        'Jeff Tong' => 'bad.com'
      }
    }
    let(:expected_email_lists) {
      {
        'Tim Cooker' => 't.c@apple.com',
        'Jim Li' => 'jim.l@google.com, j.li@google.com',
        'Joe lecox' => 'joe.lecox@alphasights.com'
      }
    }
    let(:expected_email_lists_with_mismatch_domain) {
      {
        'Tim Cooker' => 't.c@apple.com',
        'Jim Li' => 'jim.l@google.com, j.li@google.com',
        'Joe lecox' => 'joe.lecox@alphasights.com',
        'Jeff Tong' => no_pattern_found_info
      }
    }

    it 'predicts the email addresses for a name domain list' do
      expect(predict_all_emails(fake_advisors)).to eq(expected_email_lists)
    end

    it 'predicts the email addresses with mismatch domain info' do
      expect(predict_all_emails(fake_advisors_with_mismatch_domain)).to eq(expected_email_lists_with_mismatch_domain)
    end
  end
end