advisors = {
  'Peter Wong' => 'alphasights.com',
  'Craig Silverstein' => 'google.com',
  'Steve Wozniak' => 'apple.com',
  'Barack Obama' => 'whitehouse.gov'
}

$email_data = {
  'John Ferguson' => 'john.ferguson@alphasights.com',
  'Damon Aw' => 'damon.aw@alphasights.com',
  'Linda Li' => 'linda.li@alphasights.com',
  'Larry Page' => 'larry.p@google.com',
  'Sergey Brin' => 's.brin@google.com',
  'Steve Jobs' => 's.j@apple.com'
}

def predict_all_emails(advisors)
  all_emails = {}
  advisors.each { |name, domain| all_emails[name] = predict_email(name, domain) }
  all_emails
end

def predict_email(name, domain)
  patterns = get_name_patterns(domain)
  first_name, last_name = name.downcase.split(' ')
  return patterns if patterns.is_a? String

  email_predicted = patterns.inject([]) do |res, item|
    res << case item
      when :first_name_dot_last_name then "#{first_name}.#{last_name}@#{domain}"
      when :first_name_dot_last_initial then "#{first_name}.#{last_name[0]}@#{domain}"
      when :first_initial_dot_last_name then "#{first_name[0]}.#{last_name}@#{domain}"
      when :first_initial_dot_last_initial then "#{first_name[0]}.#{last_name[0]}@#{domain}"
    end
  end
  email_predicted.join(', ')
end

def get_name_patterns(domain)
  patterns = Array.new
  $email_data.values.select { |email| email.include? domain }.each do |email|
    first_name, last_name = email.split('@')[0].split('.')
    pattern = first_name.length > 1 ? 'first_name' : 'first_initial'
    pattern << '_dot_'
    pattern << (last_name.length > 1 ? 'last_name' : 'last_initial')
    patterns << pattern.to_sym
  end
  return patterns.uniq unless patterns.empty?

  "email address cannot be predicted; No email with domain name: #{domain} found in dataset"
end

p predict_all_emails(advisors)
