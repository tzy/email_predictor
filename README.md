Run email_predictor step by step:
=================
###1. Clone this repository into a directory of your choice.
> $ git clone https://tzy@bitbucket.org/tzy/email_predictor.git

###2. Install Ruby and RSpec.
[ruby-lang.org](https://www.ruby-lang.org/en/installation/) has instructions on how to install Ruby.

install RSpec:
> $ gem install rspec

###3. To run rspec tests, run email_predictor_spec.rb
From inside the 'email_predictor' directory, run:
> $ rspec email_predictor_spec.rb

the terminal output should be:
> {"Peter Wong"=>"peter.wong@alphasights.com", "Craig Silverstein"=>"craig.s@google.com, c.silverstein@google.com", "Steve Wozniak"=>"s.w@apple.com", "Barack Obama"=>"email address cannot be predicted; No email with domain name: whitehouse.gov found in dataset"}
> .........

> Finished in 0.00459 seconds (files took 0.23596 seconds to load)
> 
> 9 examples, 0 failures


###4. To run the email predictor script, run email_predictor.rb.
From inside the 'email_predictor' directory, run:
> $ ruby email_predictor.rb

it will print out a hash with predict emails for the 'advisors' hash in email_predictor.rb:
>{"Peter Wong"=>"peter.wong@alphasights.com", "Craig Silverstein"=>"craig.s@google.com, c.silverstein@google.com", "Steve Wozniak"=>"s.w@apple.com", "Barack Obama"=>"email address cannot be predicted; No email with domain name: whitehouse.gov found in dataset"}